#include <iostream>
#include "venda.hpp"
#include "estoque.hpp"
#include "pesquisa.hpp"
#include "recomendacao.hpp"
#include "carrinho.hpp"


using namespace std;

class Menu {

public:
	Menu();
	~Menu();
	void MenuOpcoes();
	int getOpcao();
	int setOpcao(int escolha);

	

protected:
	int escolha;
};
#include <iostream>
#include <string>
#include <fstream>
#include <string.h>
#include <vector>
using namespace std;

class ModoEstoque {

    public:
    ModoEstoque();
    ~ModoEstoque();
    void TelaInicial();
    void CadastrarCategoria();
    void CadastrarProduto();
    void setProduto(string produto);
    void setPreco(float pr);
    void setCategoria(string cat);
    int getQuantidade();
    string getProduto();
    void MenuOpcoes();
    void Atualizar();
    string getCategoria();
    void setQuantidade(int qt);


    
    private:
    float preco;
    string categoria;
    string produto;
    int escolha;
    int quantidade;
    string temp;
    vector<string>att;
    vector<string>novo;

    


};

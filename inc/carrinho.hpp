#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include "cliente.hpp"
using namespace std;

class Carrinho : public Cliente{
  public:
  Carrinho();
  ~Carrinho();
  void AdicionarCarrinho(string email, string produto);
  void ConsultarCarrinho();
  void LimpaCarrinho(string email);
  void FinalizaCartao(string email, float total);
  void FinalizarCompra(string email, float total);
  void FinalizaBoleto(string email, float total);
  void CompraSCadastro(string produto);

  private:
  int escolha;
  vector<string>product;
  vector<string>limpart;
  vector<string>limpeza;
  vector<string>ver;
  vector<string>qtd2;
  string prod;
  vector<float> qtd;
  string numerocartao;
  string datavalidade;
  string cvv;
  string temp;
  

};
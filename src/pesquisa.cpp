#include "menu.hpp"
#include <cctype>
#include <algorithm>

Pesquisa::Pesquisa(){
    vector<string>vet = {};
    vector<int> qtd = {};
}



void Pesquisa::Pesquisar(){
    system("clear");
    cout << "                                  PESQUISAR" << endl << endl << endl;
    cout << "1- Pesquisar produto " << endl;
    cout << "2- Pesquisar categoria " << endl;
    cout << endl;
    cout << "Selecione uma das opções acima: ";
    cin >> escolha;

    switch (escolha){
        case 1:
        PesquisarProduto();
        break;
        case 2:
        PesquisarCategoria();
        break;
    }

}

void Pesquisa :: PesquisarProduto(){
    system("clear");
    
    cout << "                                  PESQUISAR" << endl << endl << endl;
    cout << "Insira o nome do produto: ";
    ModoEstoque p;
    string temp;
    cin.ignore();
    getline(cin,temp);
    p.setProduto(temp);
    temp.erase(remove(temp.begin(), temp.end(), ' '), temp.end());
    transform(temp.begin(), temp.end(), temp.begin(), ::tolower); 
    ifstream consulta;
    consulta.open("../ep1/doc/Produtos/" + temp + ".txt");
    if(consulta.fail() == 1){
        system ("clear");
        cout << "                                  PESQUISAR" << endl << endl << endl;
        cout << "Produto não encontrado na base de dados. ";
    }else {

    
    while (getline(consulta, pesquisa))
	{
		if(pesquisa.size() > 0)
		vet.push_back(pesquisa);
	}

    for (int i = 0; i < 2; i++){
    stringstream parser(vet[i]);
    float x = 0;

    parser >> x;

    qtd.push_back(x);

    }

    system("clear");
    cout << "                                  PESQUISAR" << endl << endl << endl;
    cout << endl;
    cout << "Produto: " << p.getProduto() << endl;
    cout << "Quantidade em estoque: " << qtd [0] << endl;
    cout << "Preço: " << qtd [1] << endl;
    cout << endl;
    cout << "0- Voltar ao menu anterior " << endl;
    cout << "1- Adicionar ao carrinho " << endl;
    cout << "Selecione uma das opções acima: ";
    cin >> escolha;

    Cliente novo;

    switch (escolha){
        case 0:
        Pesquisar();
        break;
        case 1:
        novo.VerificadorCliente(temp);
        break;
    }


    consulta.close();
    }
    
}

void Pesquisa::PesquisarCategoria(){
    system ("clear");
    cout << "                                  PESQUISAR" << endl << endl << endl;
    cout << "Insira o nome da categoria: ";
    string temp;
    cin.ignore();
    getline(cin, temp);
    ModoEstoque novo;
    temp.erase(remove(temp.begin(), temp.end(), ' '), temp.end());
    transform(temp.begin(), temp.end(), temp.begin(), ::tolower); 
    novo.setCategoria(temp);
    ifstream consulta;
    consulta.open("../ep1/doc/Categorias/" + novo.getCategoria() + ".txt");
    if(consulta.fail() == 1){
        system ("clear");
        cout << "                                  PESQUISAR" << endl << endl << endl;
        cout << "Produto não encontrado na base de dados. " << endl;
    } 
    else {
    getline(consulta, pesquisa, '\0');
    system("clear");
    cout << "                                  PESQUISAR" << endl << endl << endl;
    cout << "Produtos encontrados na categoria: " << endl;
    cout << pesquisa;
    cout << endl;
    cout << "Digite 0 para voltar ao menu anterior ou 1 para adicionar algum produto da categoria ao carrinho: ";
    cin >> escolha;

    if (escolha == 0){
        Pesquisar();
    }
    else if (escolha == 1){
        cout << endl;
        cout << "Insira o nome do produto que deseja adicionar ao carrinho: ";
        cin.ignore();
        getline (cin, temp);
        temp.erase(remove(temp.begin(), temp.end(), ' '), temp.end());
        transform(temp.begin(), temp.end(), temp.begin(), ::tolower); 
        novo.setProduto(temp);
        ifstream carrinho;
        carrinho.open ("../ep1/doc/Produtos/" + temp + ".txt");
        if(carrinho.fail() == 1){
            cout << "Produto não encontrado na base de dados. " << endl;
        }
        else {
            Cliente novo;
            novo.VerificadorCliente(temp);
        }
    }


    }

}

void Pesquisa :: Categorias () {
    system ("clear");
    cout << "                               CATEGORIAS" << endl << endl << endl;
    ifstream categorias;
    categorias.open ("../ep1/doc/Categorias/listacategorias.txt");
    string temp;
    getline (categorias, temp, '\0');
    cout << temp;
}

Pesquisa::~Pesquisa(){
   vet.clear();
   qtd.clear();
}
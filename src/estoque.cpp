#include <iostream>
#include <cctype>
#include <algorithm>
#include "menu.hpp"

using namespace std;

ModoEstoque::ModoEstoque(){
    vector<string>att = {};
    vector<string>novo= {};
	
}

void ModoEstoque::TelaInicial(){

    int escolha;

    system("clear");
    cout << "                                    MODO ESTOQUE" << endl << endl << endl;
    cout << "1. Criar categoria" << endl;
    cout << "2. Cadastrar produto" << endl;
    cout << "3. Atualizar estoque de produto. " << endl <<endl;
    cout << "Selecione uma das opções acima: ";
    cin >> escolha;

    switch (escolha){
        case 1:
        CadastrarCategoria();
        break;
        case 2:
        CadastrarProduto();
        break;
        case 3:
        Atualizar();
        break;
    }

    
}

ModoEstoque::~ModoEstoque(){

}

void ModoEstoque::CadastrarCategoria(){
    
system("clear");
cout << "Insira o nome da categoria a ser cadastrada: ";
cin.ignore();
getline(cin,categoria);
categoria.erase(remove(categoria.begin(), categoria.end(), ' '), categoria.end());
transform(categoria.begin(), categoria.end(), categoria.begin(), ::tolower); 

ifstream arquivo;
arquivo.open ("../ep1/doc/Categorias/" + categoria + ".txt");

    if (arquivo.fail() == 1){
        ofstream arquivo;
        arquivo.open ("../ep1/doc/Categorias/" + categoria + ".txt");
        arquivo.close();
        ofstream cat;
        cat.open ("../ep1/doc/Categorias/listacategorias.txt", ios::out | ios :: app);
        cat << categoria << endl;
        cout << "Cadastrado com sucesso. " << endl << endl;
        MenuOpcoes();
    }

    else {
        cout << "Erro! :( Categoria já cadastrada." << endl;
        MenuOpcoes();
    }

    
    arquivo.close();    

}

void ModoEstoque::MenuOpcoes(){

        cout << "1- Modo Estoque. " << endl;
        cout << "2- Menu principal. " << endl;
        cout << "3- Cadastrar outra categoria. " << endl;
        cout << "4- Sair do programa. " << endl << endl;
        cout << "Escolha uma opção acima: ";
        cin >> escolha;

        switch (escolha){
            case 1:
            TelaInicial();
            break;
            case 2:
            Menu();
            break;
            case 3: 
            CadastrarCategoria();
            break;
            case 4:
            exit(0);
            break;
        }

}

void ModoEstoque::CadastrarProduto(){
    system ("clear");
    cout << "Insira a categoria do produto: ";
    cin.ignore();
    getline(cin,categoria);
    categoria.erase(remove(categoria.begin(), categoria.end(), ' '), categoria.end());
    transform(categoria.begin(), categoria.end(), categoria.begin(), ::tolower); 
    ifstream arquivo;
    arquivo.open("../ep1/doc/Categorias/" + categoria + ".txt");
    if (arquivo.fail() == 1){
        cout << "Categoria não cadastrada. " << endl << endl;
        MenuOpcoes();
    }
    else {
        cout << "Insira o nome do produto: ";
        getline(cin,produto);
        fflush(stdin);
        produto.erase(remove(produto.begin(), produto.end(), ' '), produto.end());
        transform(produto.begin(), produto.end(), produto.begin(), ::tolower); 
        ifstream p;
        p.open("../ep1/doc/Produtos/" + produto + ".txt");
        if (p.fail() == 1){
        fflush(stdin);
        cout << "Insira a quantidade a ser cadastrada: ";
        cin >> quantidade;
        cout << "Insira o preço do produto: ";
        cin >> preco;
        ofstream arquivo;
        ofstream prod;
        arquivo.open("../ep1/doc/Categorias/" + categoria + ".txt", ios::ate|ios::app|ios::out);
        prod.open ("../ep1/doc/Produtos/" + produto + ".txt", ios::ate |ios:: app| ios :: out);
        arquivo << produto << endl;
        arquivo.close();
        prod << quantidade << endl << preco << endl << categoria << endl;
        prod.close();
        cout << endl;
        cout << "Produto cadastrado com sucesso!" << endl <<endl;
        MenuOpcoes();
        } else {
        	cout << "Produto já cadastrado " << endl;
        	cout << endl;
        	MenuOpcoes();
        }     
        
    }
    
}

void ModoEstoque::Atualizar(){
	system ("clear");
	cout << "                                    MODO ESTOQUE" << endl << endl << endl;
	cout << "1- Atualizar quantidade de um produto " << endl;
	cout << "2- Atualizar preço de um produto " << endl;
	cout << endl;
	cout << "Selecione uma das opções acima: ";
	cin >> escolha;

	if (escolha == 1){
		system ("clear");
		cout << "                                    MODO ESTOQUE" << endl << endl << endl;
		cout << "Insira o nome do produto: ";
        cin.ignore();
		getline (cin, produto);
		cout << "Insira a nova quantidade do produto: ";
		cin >> quantidade;
        produto.erase(remove(produto.begin(), produto.end(), ' '), produto.end());
        transform(produto.begin(), produto.end(), produto.begin(), ::tolower); 
        ifstream atualizar;
        atualizar.open("../ep1/doc/Produtos/" + produto + ".txt");
        if (atualizar.fail() == 1){
            cout << endl;
            cout << "Produto não consta na base de dados. Encerrando... " << endl;
        }else {
        while (getline(atualizar, temp))
            {
	            if(temp.size() > 0)
		        att.push_back(temp);
            }
        atualizar.close();


        ofstream atualizar2;
        atualizar2.open("../ep1/doc/Produtos/" + produto + ".txt", ios::trunc | ios::out | ios::ate);
        atualizar2 << quantidade << endl << att[1] << endl;
        cout << endl;
        cout << "Quantidade antiga: " << att[0] << endl;
        cout << "Quantidade nova: " << quantidade << endl;
        cout << endl;
        cout << "Quantidade alterada com sucesso! " << endl;

        }
	}

    if (escolha == 2){
		system ("clear");
		cout << "                                    MODO ESTOQUE" << endl << endl << endl;
		cout << "Insira o nome do produto: ";
		cin.ignore();
        getline(cin, produto);
		cout << "Insira o novo preço do produto: ";
		cin >> preco;
        produto.erase(remove(produto.begin(), produto.end(), ' '), produto.end());
        transform(produto.begin(), produto.end(), produto.begin(), ::tolower); 
        ifstream atualizar;
        atualizar.open("../ep1/doc/Produtos/" + produto + ".txt");
        if (atualizar.fail() == 1){
            cout << "Produto não consta na base de dados. Encerrando... " << endl;
        } else {
        while (getline(atualizar, temp))
            {
	            if(temp.size() > 0)
		        att.push_back(temp);
            }

        atualizar.close();


        ofstream atualizar2;
        atualizar2.open("../ep1/doc/Produtos/" + produto + ".txt", ios::trunc | ios::out | ios::ate);
        atualizar2 << att[0] << endl << preco << endl;
        atualizar2.close();
        cout << endl;
        cout << "Preço antigo: R$" << att[1] << endl;
        cout << "Preço novo: R$" << preco << endl;
        cout << endl;
        cout << "Preço alterado com sucesso! " << endl;

	}



    }

}



void ModoEstoque::setProduto(string prod){
    this -> produto = prod;
}

string ModoEstoque :: getProduto(){

    return this -> produto;
}

void ModoEstoque::setPreco(float pr){
    preco = pr;
}

void ModoEstoque::setCategoria(string cat){
    this->categoria = cat;
}

string ModoEstoque::getCategoria(){
    return this->categoria;
}

void ModoEstoque::setQuantidade(int qt){
    quantidade = qt;
}

int ModoEstoque::getQuantidade(){
    return quantidade;
}

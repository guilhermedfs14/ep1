#include "menu.hpp"
#include <cctype>
#include <algorithm>
#include <sstream> 



Carrinho::Carrinho(){
    vector<string>product = {};
    vector<float>qtd = {};
    vector<string>limpeza = {};
    vector<string>ver = {};

}

Carrinho::~Carrinho(){
    product.clear();
    qtd.clear();
    limpeza.clear();
    ver.clear();
    
}



void Carrinho::AdicionarCarrinho(string email, string produto){
    system ("clear");
    cout << "                      CARRINHO" << endl << endl;
    ModoEstoque quantidade;
    Cliente ema;
    cout << "Insira a quantidade que deseja adicionar: ";
    int qt;
    cin >> qt;
    quantidade.setQuantidade(qt);
 
    ifstream bag;
    bag.open ("../ep1/doc/Produtos/" + produto + ".txt");

    while (getline(bag, prod))
    {
	    if(prod.size() > 0)
		    product.push_back(prod);
    }

    for (int i = 0; i < 2; i++){
        stringstream parser(product[i]);
        float x = 0;
        parser >> x;
        qtd.push_back(x);
    }

    bag.close();

    if (quantidade.getQuantidade() > qtd[0]){
        cout << endl;
        cout << "Poxa, infelizmente não há estoque suficiente! Produto não adicionado ao carrinho. " << endl;
    }
    else {
        
        ofstream bag2;
        bag2.open("../ep1/doc/Carrinhos/" + email + ".txt", ios::ate|ios::app|ios::out);
        bag2 << produto << endl << qtd[1] * quantidade.getQuantidade() << endl << quantidade.getQuantidade() << endl;
        bag2.close();

        ofstream bag;
        bag.open("../ep1/doc/Produtos/" + produto + ".txt", ios::trunc | ios::out | ios::ate);
        bag << qtd[0] - quantidade.getQuantidade() << endl << qtd[1] << endl << product[2] << endl;
        bag.close();
        cout << endl;
        cout << "Produto adicionado ao seu carrinho com sucesso! " << endl;

        ofstream rec;
        rec.open("../ep1/doc/Recomendados/" + email + ".txt", ios::ate|ios::app|ios::out);
        rec << product [2] << endl;
        rec.close();

    }

    }

void Carrinho :: ConsultarCarrinho(){
    system ("clear");
    cout << "                      CARRINHO" << endl << endl;
    cout << "Insira seu e-mail: ";
    string email;
    cin.ignore();
    getline (cin, email);
    Cliente consulta;
    email.erase(remove(email.begin(), email.end(), ' '), email.end());
    transform(email.begin(), email.end(), email.begin(), ::tolower); 
    consulta.setEmail(email);
    cout << "Insira sua senha: ";
    string senha;
    cin >> senha;
    consulta.setSenha(senha);

    if (consulta.VerificarLogin(email, senha) == 0){
        cout << "Não há carrinhos associados ao cliente. " << endl;
    } else {
        if (consulta.VerificarLogin(email, senha) == 1){
            cout << "E-mail e/ou senha incorreto(s). " << endl;

        }
        else {
            if (consulta.VerificarLogin(email, senha) == 2){
            cout << endl;
            ifstream verifica;
            verifica.open ("../ep1/doc/Carrinhos/" + email + ".txt");
            string temp2;

            if (verifica.fail() == 1){
            	cout << "Não foram encontrados carrinhos associados ao usuário. ";
            }else{

	            while (getline(verifica, temp2))
	            {
		            if(temp2.size() > 0)
			        product.push_back(temp2);
	            }

	            verifica.close();

	            for (unsigned int i = 1; i < product.size()-1; i+=3){
	                stringstream parser(product[i]);
	                float x = 0;
	                parser >> x;
	                qtd.push_back(x);
	            }

	            float total = 0;

	            for (unsigned int i = 0; i < product.size() - 1; i++){
	                total += qtd[i];
	            }


	            for (unsigned int i = 0; i < product.size()-1; i+=3){
	                cout << endl;
	                cout << "Nome: " << product [i] << endl;
	                cout << "Preço: R$" << product [i+1] << endl;
	                cout << "Quantidade: " << product [i+2] << endl;
	            }

	            cout << endl;
	            ifstream socio;
	            socio.open("../ep1/doc/Cadastros/" + email + ".txt");
	            string temp3;
	            while (getline(socio, temp3))
	            {
		            if(temp3.size() > 0)
			        ver.push_back(temp3);
	            }
	            verifica.close();

	            stringstream parser(ver[4]);
	            int x = 0;
	            parser >> x;

	            if (x == 1){
	            float desconto = total * 0.15;
	            total = total - desconto;
	            cout <<"Total do carrinho (Sócio): R$" << total << endl;
	            cout << "Total do desconto (15%): R$" << desconto << endl; 
	            cout << endl;
	            cout << "Digite 0 para limpar o carrinho e 1 para finalizar a compra: ";
	            int escolha;
	            cin >> escolha;

	            switch (escolha){
	                case 0:
	                LimpaCarrinho(email);
	                break;
	                case 1:
	                FinalizarCompra(email, total);
	                break;
	            }
	            } else {
	            cout <<"Total do carrinho (Não-Sócio): R$" << total << endl;
	            cout << "Total do desconto: R$" << total*0; 
	            cout << endl;
	            cout << "Digite 0 para retornar ao menu anterior e 1 para finalizar a compra: ";
	            int escolha;
	            cin >> escolha;
                ModoVenda TelaInicial;
	            switch (escolha){
	                case 0:
                    TelaInicial.TelaInicialMV();
	                break;
	                case 1:
	                FinalizarCompra(email, total);
	                break;
	            }
	            }
	            

	        }

    }
    }
	}


}

void Carrinho :: FinalizarCompra(string email, float total){
    system ("clear");
    cout << "                         FINALIZAR COMPRA" << endl << endl << endl;
    cout << "É necessário o preenchimento de algumas informações de pagamento. " << endl;
    cout << "Aviso: Esse é um software de teste. Todos os produtos, marcas e formas de pagamento são fictícios. " << endl;
    cout << "Primeiro, é necessário que escolha uma das opções de pagamento: " << endl << endl;
    cout << "1 - Cartão de Débito/Crédito" << endl;
    cout << "2 - Boleto Bancário" << endl;
    cout << endl;
    cout << "Selecione uma das opções acima: ";
    int escolha;
    cin >> escolha;

    switch (escolha){
        case 1:
        FinalizaCartao(email, total);
        break;
        case 2:
        FinalizaBoleto(email, total);
        break;
    }
}

void Carrinho :: FinalizaCartao (string email, float total){
        system ("clear");
        cout << "                         FINALIZAR COMPRA" << endl << endl << endl;
        cout << "Aviso: Nos campos a seguir, para a sua segurança, não forneça dados bancários reais. " << endl << endl;
        cout << "Insira os números do cartão: ";
        cin.ignore();
        getline(cin, numerocartao);
        cout << numerocartao[0];
        cout << numerocartao.size();

        if (numerocartao.size() == 16 && (numerocartao[0]=='5' || numerocartao[0]=='4')){
            cout << "Insira a data de validade do cartão (4 digitos): ";
            getline (cin, datavalidade);
            if (datavalidade.size() == 4){
                cout << "Insira o CVV do cartão: ";
                cin >> cvv;
                if (cvv.size() == 3){
                    ofstream finalizar;
                    temp = to_string(total);
                    finalizar.open("../ep1/doc/Compras/" + email + ".txt", ios::ate | ios :: app | ios::ate);
                    finalizar << total << endl << numerocartao << endl << datavalidade << endl << cvv << endl;
                    finalizar.close();
                    temp = "../ep1/doc/Carrinhos/" + email + ".txt";
                    int n = temp.size();
                    char *remov;
		            remov = (char*) malloc (n+1);
                    strcpy(remov, temp.c_str());
                    remove(remov);
                    system ("clear");
                    cout << "                               FINALIZAR COMPRA" << endl << endl << endl;
                    cout << "Compra finalizada com sucesso! Em breve você receberá o produto em sua casa. " << endl;
                    exit(0);
                    free(remov);

                } else{
                    cout << "CVV inválido." << endl;
                    cout << "Entre no menu e tente novamente. " << endl;
                    exit(0);
                }
            
            }else {
                cout << "Data de validade inválida. ";
                cout << "Entre no menu e tente novamente. " << endl;
                exit(0);
            }
        } else {
            cout << "O número do cartão informado é inválido." << endl;
            cout << "Entre no menu e tente novamente. " << endl;
            exit (0);
        }

        
    

}

void Carrinho :: FinalizaBoleto(string email, float total){
        cout << "Aviso: Os números gerados são aleatórios. Não há intencão alguma adquirir vantagem de modo ilicito. " << endl << endl;
        srand (time(NULL));
        int boleto = (rand() % 100000000000) * (rand() % 100000000000) * (rand() % 10000000000) * (rand() % 10000);
        srand (time(NULL));
        int validade = rand()%10000;
        string temp1;
        string tempo;
        tempo = to_string(boleto);
        temp1 = to_string(validade);
        ofstream finalizar;
        temp = to_string(total);
        finalizar.open("../ep1/doc/Compras/" + email + ".txt", ios::ate | ios :: app | ios::ate);
        finalizar << total << endl << tempo << endl << temp1 << endl << "000" << endl;
        finalizar.close();
        temp = "../ep1/doc/Carrinhos/" + email + ".txt";
        int n = temp.size();
        char *remov;
		remov = (char*) malloc (n+1);
        strcpy(remov, temp.c_str());
        remove(remov);
        system ("clear");
        cout << "                               FINALIZAR COMPRA" << endl << endl << endl;
        cout << "Compra finalizada com sucesso! Em breve você receberá o produto em sua casa. " << endl;
        cout << "Código de barras : " << tempo << endl;
        cout << "Total: R$" << total << endl;
        exit(0);
        free(remov);

        cin.ignore();
}

void Carrinho :: LimpaCarrinho(string email){

    Cliente l;
    ifstream lim;
    lim.open("../ep1/doc/Carrinhos/" + email + ".txt");
    limpart.clear();
    string tempo;
    while(getline(lim, tempo)){
        if(tempo.size() > 0)
		    limpart.push_back(tempo);
        }

    lim.close();

    limpeza.clear();

    for (unsigned int i = 0; i < limpart.size() - 1; i+=3){
        ifstream recuperar;
        recuperar.open("../ep1/doc/Produtos/" + limpart [i] + ".txt");
        
        string temp2;

        while(getline(recuperar, temp2)){
        if(temp2.size() > 0)
		    limpeza.push_back(temp2);
        }

        recuperar.close();

        stringstream parser(limpart[i+2]);
            int x = 0;
            parser >> x;
        
        stringstream novo(limpeza[0]);
            int y = 0;
            novo >> y;
        string temp4 = limpart [i];
        ofstream regravar;
        regravar.open("../ep1/doc/Produtos/" + temp4 + ".txt", ios::trunc|ios::out|ios::app);
        cout << limpart[i];
        if (regravar.fail() == 1){
            cout << "quebrou";
        }
        regravar << y + x << endl << limpeza [1] << endl << limpeza [2] << endl;
        regravar.close();
        
    }
        /*string car = "../ep1/doc/Carrinhos/" + email + ".txt";
        int v = car.size();
        char *remov;
		remov = (char*) malloc (v+1);
        strcpy(remov, car.c_str());
        remove(remov);*/
        
}

void Carrinho:: CompraSCadastro(string produto){
cout << "                          COMPRA SEM CADASTRO" << endl << endl << endl;
system ("clear");
    ModoEstoque quantidade;
    Cliente ema;
    cout << "Insira seu CPF: ";
    string cpf;
    cin >> cpf;
    cout << "Insira a quantidade que deseja adicionar: ";
    int qt;
    cin >> qt;
    quantidade.setQuantidade(qt);
 
    ifstream bag;
    bag.open ("../ep1/doc/Produtos/" + produto + ".txt");

    while (getline(bag, prod))
    {
	    if(prod.size() > 0)
		    product.push_back(prod);
    }

    for (int i = 0; i < 2; i++){
        stringstream parser(product[i]);
        float x = 0;
        parser >> x;
        qtd.push_back(x);
    }

    bag.close();

    if (quantidade.getQuantidade() > qtd[0]){
        cout << endl;
        cout << "Poxa, infelizmente não há estoque suficiente! Produto não adicionado ao carrinho. " << endl;
    }
    else {

        float total = qtd[1] * quantidade.getQuantidade();
        
    
        ofstream bag;
        bag.open("../ep1/doc/Produtos/" + produto + ".txt", ios::trunc | ios::out | ios::ate);
        bag << qtd[0] - quantidade.getQuantidade() << endl << qtd[1] << endl << product[2] << endl;
        bag.close();
        cout << endl;
        cout << "Produto adicionado ao seu carrinho com sucesso! " << endl;

        FinalizarCompra(cpf, total);

    }

}

    
    


     

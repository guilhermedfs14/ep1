# EP1 - Orientação a Objetos - Universidade de Brasília (2019.2)

## Descrição
<p>O software tem como enfoque gerenciar as vendas de uma loja online. A loja se chama Hållbazar que deriva do termo *Hållbarhet* que em *sueco significa "sustentabilidade" em conjunção com o termo bazar. A loja é focada em vender móveis de forma sustentável, aproveitando móveis de alta qualidade como forma de reduzir o consumismo e consequentemente alterar o status quo. Os são vendidos exclusivamente online na loja e buscados no depósito pelo comprador, reduzindo os custos de envio. </p>

###### *Estocolmo, Suécia (1972) foi onde a primeira grande reunião de chefes de estado organizada pelas Nações Unidas para tratar das questões relacionadas à degradação do meio ambiente foi realizada.

## Requisitos

<p>Para executar o software, é necessário ter os pacotes *make* e as bibliotecas *vector*, *string*, *iostream*, *cctype*, *algorithm*, *fstream*, *stdlib.h*, *sstream*, *string.h* e *cstdio*.

## Modos de Operação

### Menu
- É composto por (1) Modo Venda, (2) Modo Estoque, (3) Modo Recomendação, (4) Sair do Programa.

### Modo Venda
É composto por 4 submenus: (1) Pesquisar, (2) Categorias, (3) Carrinho, (4) Minha Conta.
 #### (1) Pesquisar: Composto por 2 opções, (1.1) Pesquisar produto e (1.2) Pesquisar Categoria.
 #### (1.1)Pesquisar Produto: O usuário é levado a uma tela onde ele tem a possibilidade de pesquisar algum produto. Após isso, é apresentada uma tela onde o usuário tem a possibilidade de adicionar ao carrinho ou voltar ao menu anterior. Caso ele adicione ao carrinho, ele é levado a uma tela ontem ele tem 3 possibilidades. 
 <p>(1.1.1) Login: o usuário deve fornecer seus dados caso tenha conta cadastrada e os produtos serão adicionados ao seu carrinho com desconto de 15%.<p>
 (1.1.2) Cadastro: o usuário deve se cadastrar e ao final os produtos serão adicionados ao seu carrinho com desconto de 15%.
 <p>(1.1.3) Comprar sem cadastro: o usuário é levado a uma tela onde ele deve fornecer seu CPF e após isso é levado a uma tela onde ele deve confirmar a compra (não há desconto). Ele terá duas opções de pagamento: Cartão de Crédito/Débito ou Boleto Bancário. Haverão as seguintes validações: o número do cartão deve iniciar com 4 ou 5, a Data de Validade deve ter 4 caracteres e o Código de Validação (CVV).

 #### (1.1)Pesquisar Categoria: O usuário é levado a uma tela onde ele tem a possibilidade de pesquisar alguma categoria. Após isso, é apresentada uma tela onde o usuário tem a possibilidade de adicionar algum produto da categoria ao carrinho ou voltar ao menu anterior. Caso ele adicione ao carrinho, ele é levado a uma tela ontem ele tem 3 possibilidades.

 <p>(1.2.1) Login: o usuário deve fornecer seus dados caso tenha conta cadastrada e os produtos serão adicionados ao seu carrinho com desconto de 15%.
 <p>(1.2.2) Cadastro: o usuário deve se cadastrar e ao final os produtos serão adicionados ao seu carrinho com desconto de 15%.
 <p>(1.2.3) Comprar sem cadastro: o usuário é levado a uma tela onde ele deve fornecer seu CPF e após isso é levado a uma tela onde ele deve confirmar a compra (não há desconto). Ele terá duas opções de pagamento: Cartão de Crédito/Débito ou Boleto Bancário. Haverão as seguintes validações: o número do cartão deve iniciar com 4 ou 5, a Data de Validade deve ter 4 caracteres e o Código de Validação (CVV).

 #### (2) Categorias: Nessa tela são apresentadas as categorias cadastradas.

 #### (3) Carrinho: Nessa tela serão apresentados uma tela de login, na qual o cliente deve inserir o e-mail e senha cadastrados por ele antes da compra. Após inserir e ser validado, será apresentada a ele uma tela na qual ele pode concluir a compra ou voltar ao menu anterior.

#### (4) Minha Conta: Nessa tela serão apresentadas 3 opções. (4.1) Visualizar dados da conta, (4.2) Ver compras sem cadastro, (4.3) Remover conta e desassociar-se.
 #### (4.1) Visualizar dados da conta: Nessa tela serão apresentadas todas as compras feitas pelo cliente até o momento.
 #### (4.2) Ver compras sem cadastros: Apresenta as compras feitas pelo usuário não-sócio utilizando apenas o CPF
#### (4.3) Remover conta e desassociar-se: Apresenta uma tela na qual é pedido o e-mail e senha do usuário. Após isso, são removidos o carrinho e o cadastro do usuário.

### Modo Estoque
É composto por 3 funções: (1) Cadastrar produto, (2) Cadastrar Categoria, (3) Atualizar estoque de produto.
#### (1) Cadastrar produto: Nessa tela são apresentados campos para inserir a categoria do produto a ser cadastrado, o nome, a quantidade e o preço, tais quais serão armazenados.
#### (2) Cadastrar Categoria: Nessa tela será apresentado um campo no qual o usuário deverá inserir o nome da categoria a ser cadastrada, tal qual será armazenada.
#### (3) Atualizar estoque de produto: Nessa tela serão apresentadas duas opções. (3.1) Atualizar quantidade de produto, (3.2) Atualizar preço de produto.
 
 #### (3.1) Atualizar quantidade de um produto: Nessa tela deverá ser inserida a nova quantidade de um determinado produto.
 #### (3.2) Atualizar preço de um produto: Nessa tela deverá ser inserido o novo preço de um determinado produto;

 ### Modo Recomendação

 Serão apresentados campos nos quais o usuário deve inserir e-mail e senha. Somente socios têm direito ao modo recomendação. Após isso, serão apresentadas 3 opções de produtos de acordo com as compras feitas pelo usuário.

 ### Sair do programa
 O programa será encerrado.






